/**
 * @file
 * {}
 */

;(function ($) {
  if (Drupal.ReferencesDialog) {
    Drupal.ReferencesDialog.invokeCallback = function (callback, entity_info, settings) {
      if (typeof settings == 'object') {
        entity_info.settings = settings;
      }
      if (callback.indexOf('/references-dialog-select') !== -1) {
        var selector = $('[name="' + settings.element_name + '"]').attr('id');
        entity_info.settings.form_build_id = $('#' + selector).parents('form').find('input[name="form_build_id"]').val();
        $.ajax({
          method: 'POST',
          url: callback,
          data: entity_info,
          success: function(response, status) {
            if (!response) {
              return;
            }
            var $html = $(response).find('[id^=' + selector + ']');
            $('#' + selector).replaceWith($html);
            if (Drupal.behaviors.chosen) {
              $('#' + selector.replace(/-/g, '_') + '_chosen').remove();
              Drupal.behaviors.chosen.attach($html.parent(), Drupal.settings);
            }
            // Change edit link href. TODO: re-factor it.
            var parent_id = $('[name="' + settings.element_name + '"]').parent().parent().attr('id');
            if ($('#' + parent_id + ' .dialog-links .edit-dialog').length != undefined) {
              var id = $('#' + parent_id + ' select').val(), param = $('.dialog-links .edit-dialog').attr('href').split(
                  '/');
              $('.dialog-links .edit-dialog').attr('href', '/' + param[1] + '/' + id + '/' + param[3]);
            }
            // Update field behaviours after re-render.
            Drupal.attachBehaviors(document, Drupal.settings);
          }
        })
      }
      else {
        $.post(callback, entity_info);
      }
    }
  }
})(jQuery);